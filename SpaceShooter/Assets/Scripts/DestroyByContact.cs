﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyByContact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    private PlayerController player;
    public int scoreValue;
    private GameController gameController;

    public GameObject oreSample;

    private int health = 200;

    [HideInInspector]
    public Slider healthBar;

	// Use this for initialization
	void Start () {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        gameController = gameControllerObject.GetComponent<GameController>();
        GameObject playerObject = GameObject.FindWithTag("Player");
        player = playerObject.GetComponent<PlayerController>();
        healthBar = GameObject.FindGameObjectWithTag("HealthBar").GetComponent<Slider>();
	}

    // Update is called once per frame
    void Update() {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Projectile"))
        {
            health -= player.damage;
            Destroy(other.gameObject);            
            if (health <= 0)
            {
                gameController.AddScore(scoreValue);
                Destroy(this.gameObject);
                Instantiate(explosion, transform.position, transform.rotation);
                if (player.defenseSystemActive)
                {
                    player.AddAsteroid();
                }
                Quaternion spawnRotation = Quaternion.identity;
                int chance = Random.Range(0, 3);
                if (chance == 2)
                {
                    Instantiate(oreSample, transform.position, spawnRotation);
                }
            }
        }

        if (other.CompareTag("Barrier"))
        {
            health -= player.damage*2;
            Destroy(other.gameObject);
            gameController.AddScore(scoreValue);
            if (other.gameObject.GetComponent<miniMover>().orbitBool)
            {
                player.asteroidShieldArray.Remove(other.gameObject);
            }
            if (health <= 0)
            {
                Destroy(this.gameObject);
                Instantiate(explosion, transform.position, transform.rotation);
            }
        }

        if (other.CompareTag("Player"))
        {
            if (player.shieldHealth == 2)
            {
                player.shield.GetComponentInChildren<Light>().color = Color.blue;
                player.shieldHealth--;
            }
            else if (player.shieldHealth == 1)
            {
                player.shieldHealth--;
                player.shield.GetComponentInChildren<Light>().enabled = false;
            }
            else
            {
                player.health--;
                print(player.health);
                healthBar.value++;
                Destroy(this.gameObject);
                Destroy(GameObject.FindGameObjectWithTag("Shield"));
                gameController.GameOver();
                Instantiate(explosion, transform.position, transform.rotation);
                if (player.health <= 0)
                {
                    Destroy(other.gameObject);
                    Instantiate(playerExplosion, transform.position, transform.rotation);
                }
               
            }
        }
    }
}
