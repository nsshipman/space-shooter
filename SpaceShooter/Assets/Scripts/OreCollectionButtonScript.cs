﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OreCollectionButtonScript : MonoBehaviour {

    GameController gameController;

	// Use this for initialization
	void Start () {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CheckCredits(int tier)
    {
        if (gameController.credits >= 100 && tier == 1)
        {
            gameController.AddScore(-100);
            GetComponentInChildren<Text>().text = "---";
            GetComponent<Button>().enabled = false;
        }

        if (gameController.credits >= 200 && tier == 2)
        {
            gameController.AddScore(-200);
            GetComponentInChildren<Text>().text = "---";
            GetComponent<Button>().enabled = false;
        }
    }
}
