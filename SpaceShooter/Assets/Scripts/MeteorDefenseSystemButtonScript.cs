﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeteorDefenseSystemButtonScript : MonoBehaviour {

    GameController gameController;

    // Use this for initialization
    void Start () {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CheckCredits(int tier)
    {
        if (tier == 1 && gameController.credits >= 100)
        {
            gameController.AddScore(-100);
            GetComponentInChildren<Text>().text = "---";
            GetComponent<Button>().enabled = false;
        }
        
        if (tier == 2 && gameController.credits >= 200)
        {
            gameController.AddScore(-200);
            GetComponentInChildren<Text>().text = "---";
            GetComponent<Button>().enabled = false;
        }
    }
}
