﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject hazard;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    private int meteorsThisWave = 5;

    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;

    private bool gameOver;
    private bool restart;
    public int credits;
    private int creditsThisWave;
    private int meteorsDestroyed;

    [HideInInspector]
    public float shotsMissed;
    [HideInInspector]
    public float shotsFired;

    public GameObject nextWaveButton;
    public GameObject shopButton;
    public GUIText reportText;
    public GameObject shopMenu; 

    public GameObject playerObject;
    public PlayerController player;


    // Use this for initialization
    void Start()
    {
        credits = 0;
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        UpdateScore();
        StartCoroutine(SpawnWaves());

        player = playerObject.GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
		if (restart)
        {
            if (Input.GetKeyDown (KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            credits += 1000;
            UpdateScore();
        }
	}

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        for (int i = 0; i < meteorsThisWave; i++)
        {
            Vector3 spawnPosition = new Vector3(
            Random.Range(-spawnValues.x, spawnValues.x),
            spawnValues.y,
            spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(hazard, spawnPosition, spawnRotation);
            yield return new WaitForSeconds(spawnWait);
        }
        yield return new WaitForSeconds(3.0f);
        CreditReport();
        meteorsThisWave += Mathf.RoundToInt(meteorsThisWave * 1.3f);
        StopCoroutine(SpawnWaves());
        playerObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        playerObject.GetComponent<PlayerController>().enabled = false;
        foreach (GameObject i in GameObject.FindGameObjectsWithTag("Ore"))
        {
            Destroy(i);
        }
        
    }

    void CreditReport()
    {
        List<GameObject> asteroidList = playerObject.GetComponent<PlayerController>().asteroidShieldArray;
        nextWaveButton.SetActive(true);
        shopButton.SetActive(true);
        reportText.gameObject.SetActive(true);
        reportText.text = "Day Complete";

        reportText.transform.GetChild(0).GetComponent<GUIText>().text = "Meteors Destroyed:                            " + meteorsDestroyed * 10;

        if (1.0f - (shotsMissed / shotsFired) > 0.8f)
        {
            reportText.transform.GetChild(1).GetComponent<GUIText>().text = "Precision Bonus:                     " + 100;
            creditsThisWave += 100;
        }
        else if ((1.0f - shotsMissed / shotsFired) > 0.5f) {
            reportText.transform.GetChild(1).GetComponent<GUIText>().text = "Precision Bonus:                     " + 50;
            creditsThisWave += 50;
        }
        else if ((1.0f - shotsMissed / shotsFired) > 0.3f)
        {
            reportText.transform.GetChild(1).GetComponent<GUIText>().text = "Precision Bonus:                     " + 20;
            creditsThisWave += 20;
        }
        else
        {
            reportText.transform.GetChild(1).GetComponent<GUIText>().text = "Precision Bonus:                     " + 0;
            creditsThisWave += 0;
        }
        shotsFired = 0;
        shotsMissed = 0;

        int damagePenalty = 10 - playerObject.GetComponent<PlayerController>().health;
        reportText.transform.GetChild(2).GetComponent<GUIText>().text = "Insuarance Cost:                            " + -(5 * damagePenalty) + "%";
        creditsThisWave = Mathf.RoundToInt(creditsThisWave * (1 - (damagePenalty * 0.05f)));
        player.health = 10;

        reportText.transform.GetChild(3).GetComponent<GUIText>().text = "Meteors Missed Tax:                            " + (meteorsDestroyed - meteorsThisWave) * 5;
        print("git gud tax");
        creditsThisWave += (meteorsDestroyed - meteorsThisWave) * 5;

        reportText.transform.GetChild(4).GetComponent<GUIText>().text = "Total Credits Earned:                            " + creditsThisWave;
        credits += creditsThisWave - (meteorsDestroyed * 10);
        UpdateScore();
    }

    public void StartNextWave()
    {
        creditsThisWave = 0;
        meteorsDestroyed = 0;
        UpdateScore();
        reportText.gameObject.SetActive(false);
        shopButton.SetActive(false);
        shopMenu.SetActive(false);
        StartCoroutine(SpawnWaves());
        player.enabled = true;
        if (player.shieldSystemActive)
        {
            player.RechargeShields();
            player.shield.GetComponentInChildren<Light>().enabled = true;
        }
        
    }

    public void ShopMenu()
    {
        reportText.gameObject.SetActive(false);
        shopMenu.SetActive(true);
    }

    public void AddScore(int newCreditValue)
    {
        meteorsDestroyed++;
        credits += newCreditValue;
        creditsThisWave += newCreditValue;
        UpdateScore();
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over";
        gameOver = true;
        restart = true;
        restartText.text = "Press 'R' for Restart";
    }

    void UpdateScore()
    {
        scoreText.text = credits + " Credits";
    }
}
