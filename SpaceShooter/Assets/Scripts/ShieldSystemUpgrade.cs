﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldSystemUpgrade : MonoBehaviour {

    [HideInInspector]
    public PlayerController player;
    [HideInInspector]
    public GameController gameController;
    public GameObject playerObject;
   
    // Use this for initialization
    void Start () {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CheckCredits(int tier)
    {
        if (gameController.credits >= 100 && tier == 1)
        {
            gameController.AddScore(-100);
            player.shield.GetComponentInChildren<Light>().enabled = true;
            player.ActivateShieldSystem();
            GetComponentInChildren<Text>().text = "---";
            return;
        }
        if (gameController.credits >=200 && tier == 2)
        {
            gameController.AddScore(-200);
            player.shield.GetComponentInChildren<Light>().color = Color.red;
            GetComponentInChildren<Text>().text = "---";
            player.ActivateShieldSystem();
        }
    }
}
