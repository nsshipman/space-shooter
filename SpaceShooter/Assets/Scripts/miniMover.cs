﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class miniMover : MonoBehaviour {

    public Rigidbody playerBody;

    //this portion and the portion below were taken from: http://answers.unity3d.com/questions/463704/smooth-orbit-round-object-with-adjustable-orbit-ra.html
    public Vector3 axis = Vector3.up;
    private float radius = 2.0f;
    private float radiusSpeed = 10.0f;
    private float rotationSpeed = 200.0f;
    [HideInInspector]
    public bool orbitBool = true;

    public float speed;

    // Use this for initialization
    void Start () {
        GameObject playerBodyObject = GameObject.FindWithTag("Player");
        playerBody = playerBodyObject.GetComponent<Rigidbody>();
        transform.position = playerBody.GetComponent<Rigidbody>().position + new Vector3 (0.0f, 0.0f, 1.0f);
    }

    // Update is called once per frame
    void Update () {
        //borrowed from: http://answers.unity3d.com/questions/463704/smooth-orbit-round-object-with-adjustable-orbit-ra.html
        if (orbitBool)
        {
            transform.RotateAround(playerBody.position, axis, rotationSpeed * Time.deltaTime);
            var desiredPosition = (transform.position - playerBody.position).normalized * radius + playerBody.position;
            transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
            transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z); //keeps it aligned with the player on the y axis
        }        
    }

    public void Shoot()
    {
        orbitBool = false; //stop orbiting
        transform.rotation.Set(0, 0, 0, 0); //realign
        Rigidbody body = gameObject.AddComponent<Rigidbody>(); //gives it physics
        body.useGravity = false; //stops it from falling off screen
        Vector3 target = new Vector3(playerBody.position.x, 0.0f, playerBody.position.z + 10.0f);
        body.velocity = (target - transform.position).normalized * speed; //sends it forwards
        this.GetComponent<RandomRotator>().enabled = true; //makes it look pretty like the other asteroids
        transform.SetParent(null);
        Destroy(gameObject, 5);
    }
}
