﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public Boundary boundary;

    public GameObject shot;
    public GameObject miniAsteroid;
    public GameObject weaponUpgradeButton;
    public GameController gameController;

    public Transform shotSpawn;
    public float fireRate;

    [HideInInspector]
    public int health;

    public int damage;
    private float nextFire;
    [HideInInspector]
    public List <GameObject> asteroidShieldArray = new List <GameObject>();

    public bool defenseSystemActive = false;
    public bool offenseSystemActive = false;

    public int weaponTier;

    public int magnetTier = 0;

    public GameObject shield;
    public bool shieldSystemActive = false;
    public int shieldTier = 0;
    public int shieldHealth = 0;

    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        gameController = gameControllerObject.GetComponent<GameController>();
        shield = GameObject.FindWithTag("Shield");
        health = 10;
        damage = 100;
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            if (weaponTier < 3)
            {
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
                GetComponent<AudioSource>().Play();
                gameController.shotsFired++;
            }
            else if ((weaponTier == 3) && (Time.time > nextFire))
            {
                nextFire = Time.time + fireRate;
                StartCoroutine(BurstShot());
            }
            else if (weaponTier == 4)
            {
                Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
                GetComponent<AudioSource>().Play();
                gameController.shotsFired++;
            }

        }
        if (Input.GetButtonDown("Fire2"))
        {
            ShootAsteroid();
        }
    }


    IEnumerator BurstShot()
    {
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(0.05f);
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            GetComponent<AudioSource>().Play();
            gameController.shotsFired++;
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        GetComponent<Rigidbody>().velocity = movement * speed;

        
        GetComponent<Rigidbody>().position = new Vector3
        (
            Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
        );

        GetComponent<Rigidbody>().rotation = Quaternion.Euler(0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
    }

    public void AddAsteroid()
    {        
        GameObject newMiniAsteroid = Instantiate(miniAsteroid, transform.position, transform.rotation);
        newMiniAsteroid.transform.parent = this.transform;
        asteroidShieldArray.Add(newMiniAsteroid);
    }

    public void ShootAsteroid()
    {
        if (asteroidShieldArray[0] != null && offenseSystemActive)
        {
            asteroidShieldArray[0].GetComponent<miniMover>().Shoot();
            asteroidShieldArray.RemoveAt(0);
        }
    }

    public void ActivateOffenseSystem()
    {
        offenseSystemActive = true;
    }

    public void ActivateDefenseSystem()
    {
        defenseSystemActive = true;
    }

    public void ActivateShieldSystem()
    {
        shieldSystemActive = true;
        shieldTier++;
    }

    public void ActivateMagnetSystem()
    {
        magnetTier++;
    }

    public void RechargeShields()
    {
        print(shieldTier);
        shieldHealth = shieldTier;
        if (shieldTier == 2)
        {
            shield.GetComponentInChildren<Light>().color = Color.red;
        }
        else if (shieldTier == 1)
        {
            shield.GetComponentInChildren<Light>().color = Color.blue;
        }

    }


}