﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreScript : MonoBehaviour {

    public static bool magnetModeActive = false;
    public GameObject player;
    public PlayerController playerScript;
    public GameController gameController;
    private int oreCollctionTier = 0;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        playerScript = player.GetComponent<PlayerController>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        oreCollctionTier = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (playerScript.magnetTier == 1)
        {
            if ((transform.position - player.transform.position).magnitude < 5)
            {
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, Time.deltaTime * 2);
            }
        }
        if (playerScript.magnetTier == 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, Time.deltaTime * 5);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
            gameController.AddScore(10);
        }
    }
}
