﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUpgradeButtonScript : MonoBehaviour {

    [HideInInspector]
    public PlayerController player;
    [HideInInspector]
    public GameController gameController;
    //private int weaponTier = 1;

    // Use this for initialization
    void Start () {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CheckCredits(int buttonNumber)
    {
        if (player.weaponTier == 0 && gameController.credits >= 100 && buttonNumber == 1)
        {
            player.weaponTier++;
            gameController.AddScore(-100);
            player.fireRate = 0.5f;
            GetComponentInChildren<Text>().text = "---";
        }
        else if (player.weaponTier == 1 && gameController.credits >= 150 && buttonNumber == 2)
        {
            player.weaponTier++;
            gameController.AddScore(-150);
            player.fireRate = 0.3f;
            GetComponentInChildren<Text>().text = "---";
        }
        else if (player.weaponTier == 2 && gameController.credits >= 300 && buttonNumber == 3)
        {
            player.weaponTier = 3;
            player.fireRate = 1.0f;
            gameController.AddScore(-300);
            GetComponentInChildren<Text>().text = "---";
            GetComponent<Button>().enabled = false;
        }

        else if (player.weaponTier == 3&& gameController.credits >=1000 && buttonNumber == 4)
        {
            player.damage = 5;
            player.weaponTier = 4;
            player.fireRate = 0.0f;
            gameController.AddScore(-1000);
            GetComponentInChildren<Text>().text = "---";
            GetComponent<Button>().enabled = false;
        }
    }
}
